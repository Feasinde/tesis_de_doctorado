%!TEX root = ../../main.tex

\chapter{Introduction}
The Mayan language family is %a language family 
spoken in an area covering the modern states of Guatemala, Belize, and Southern Mexico \parencite[p. 81]{law2014language}. It consists of around 30 languages grouped into five or six major sub-groups, depending on the source \parencite{campbell1985mayan,law2014language}. Most subgroups and most speakers are found today in Guatemala, where between 40-60\% of the population are native speakers \parencite{censo2018,england2003mayan}. Mayan languages are relatively healthy, %especially when compared to other native languages in the Americas, 
but their presence online and on the global scene in general is almost non-existent. In effect, Mayan languages, despite the total number of speakers, are considered to be somewhat in decline: according to \textcite{richards2003atlas}, only around half the population of ethnic Mayas are Mayan speakers, and the languages are associated in many social contexts to %such traits as 
backwardness, ignorance and poverty \parencite{england2003mayan}. 

\section{Overview of the Mayan Languages} % (fold)
\label{sec:the_mayan_languages}

\begin{figure}
  \centering
  \includegraphics[scale=.18]{chapters/intro/figs-tabs/balam.png}
  \caption{Sample of the ancient Mayan script, reading \textit{b'alam}, ``jaguar'', using a combination of the logogram and the syllabogram. Attribution: Goran tek-en under license \href{https://creativecommons.org/licenses/by-sa/4.0/deed.en}{CC BY-SA 4.0}.}
  \label{fig:balam}
\end{figure}

\begin{figure}[t]
  \centering
  \includesvg[scale=.7]{chapters/intro/figs-tabs/lenguas_mayas}
  \caption{The Mayan linguistic communities of Guatemala according to the most recent data available.}
  \label{fig:mayan_langs}
\end{figure}

The oldest attested Mayan language, referred to as Classic Maya, dates back to ca 300 BC. Written in the the Mayan script, it belongs to a tradition corresponding to the few instances in human history in which writing was independently invented, along with the systems developed in Ancient Egypt, Sumer, and Ancient China \parencite[p 762]{fagan1996oxford}. Figure~\ref{fig:balam} shows an example of the script. The Mayan script is logosyllabic: glyphs may act either as logograms, representing a complete semantic unit, or syllabograms, representing a syllable \parencite{coe2016reading}. Modern Mayan languages are written using the Latin script, with additional diacritics used to denote features such as vowel length and glottalisation, amongst others.

Indigenous peoples in Guatemala are almost exclusively Maya, with the sole exception of the Xinca people, who are culturally and linguistically distinct from the Maya. Mayan languages are held as the greatest expression of Mayan identity in the region: They are relatively healthy, especially when compared with other native languages in the Americas, as there are millions of speakers whose age groups are not confined to the elderly; there are active revitalisation and revival movements championed by the younger generations, and there is a growing community of academically trained researchers that for decades have produced most of the work in the field of Mayan linguistics \parencite{bricker2007quarter,england2003mayan}.

Much variation exists in terms of breadth and number of speakers. The three most spoken languages ---K'iche', Yucatec Mayan, and Q'eqchi--- all boast between a million and half a million native speakers, and are spoken across different geographical areas in Guatemala and Mexico  \parencite{richards2003atlas,law2014language}. In contrast, there are some languages with very few speakers and in imminent danger of language death, as is the case of Itza', which is only spoken by elderly adults and has fewer than 1\,000 native speakers \cite{censo2018,Eberhard2021ethnologue}. However, despite their size in terms of speakers and geographical extent, as shown in Figure~\ref{fig:mayan_langs}, Mayan languages are usually not given an official status in their respective countries; their use is widespread in daily activities and familial environments, but they are nearly non-existent %almost all but absent 
in matters of governance, education, mass media, and healthcare \parencite{romero2017labyrinth}. For example, in Guatemala, during %the height of the 
Covid-19 pandemic, the official online portal to register for the government-sponsored vaccination program was only accessible in Spanish \parencite{espana2021plan}; to this day it continues to lack any Mayan language version. Even when official support is said to exist, the actual implementation of educational initiatives remain deficient, as is the case with incipient government-sponsored programs to teach Yucatec Mayan in schools \parencite{bote2023yucateco}. 

Bilingualism with Spanish is very common, though not uniform across geographical, population and gender lines: isolated communities often present a high number of monolingual speakers, and men usually exhibit higher proficiency in bilingualism \parencite{Bennett2015Intro,romero2017labyrinth,richards2003atlas}. Language shift, whereby a Mayan language is displaced by either Spanish or, less commonly, another Mayan language \parencite{Bennett2015Intro,romero2012they}, occurs more quickly in urban environments, where a lingua franca is expected to be used.

Literacy is overall low as a result of decades of policy where using native languages in schools was discouraged or even punished \parencite{french2010maya}. As a result, many Mayan speakers regard the orthography of their own languages as more difficult and inaccessible than that of Spanish. Change was slowly brought about with the introduction of bilingual educational programs in the early 1990s, which finally led to a continued effort to revitalise the role of written Mayan languages. The Guatemalan Academy of Mayan Languages (ALMG), established in 1990, plays an important role in the standardisation of both the orthography of the different \emph{linguistic communities} and their corresponding spoken languages, while also engaging in literacy and publication efforts.\footnote{\url{https://www.almg.org.gt/nosotros/historia}} A similar role is played by the Proyecto Lingüístico Francisco Marroquín Foundation (PLFM),\footnote{\url{https://plfm.org/quienes-somos/historia}} also in Guatemala, and National Institute of Indigenous Languages (INALI), established in 2003, in Mexico,\footnote{\url{https://site.inali.gob.mx/Micrositios/normas/index.html}} both of which have published several grammars.

Mayan languages exhibit a high degree of dialectal variation \parencite{romero2017labyrinth}. While the most important reason for this is natural language change and historical innovation, the sprachbund of Mayan languages in Guatemala and Southern Mexico has resulted in much linguistic exchange in the forms of loanwords and calques, usually manifesting as the influence of a language with more speakers and political leverage over another with fewer speakers and less influence. Dialectal divergence within the same language is also attested, as is the case with the dichotomies of the Western and Eastern, and Standard and Lowland dialects of Q'eqchi' \parencite{dechicchis1989q,romero2012they}. Additionally, the politics of identity play a key role in delimiting the difference between a language and a dialect in the mind of their speakers, as seen in the cases of Achi, Akatek, and Chalchitek, which are sometimes considered dialects of K’iche’, Q’anjob’al, and Awakatek, respectively \parencite{Bennett2015Intro}. In general, Mayan languages exhibit limited mutual intelligibility, and code-switching with Spanish and other Mayan languages in areas of high contact is common \parencite{little2009language}.

\subsection{Phonology, Morphosyntax and Semantics} % (fold)
\label{sub:mayan_phonology_syntax_and_semantics}
Despite their relative obscurity in the field of NLP, Mayan languages are well studied and documented in matters of historical linguistics, morphosyntax, phonology, and semantics. Here we present an extremely condensed overview of the linguistics of Mayan languages for those unfamiliar with them. This section is mostly a summary of the extensive works by \textcite{Bennet2015phonology}, \textcite{coon2015morphosyntax}, and \textcite{henderson2015semantics}.

\subsubsection{Phonology} % (fold)
\label{ssub:phonology}
Most Mayan languages distinguish ten vowel phonemes by contrasting /a, e, i, o, u/ as either short or long. Vowel length is usually conditioned by stress. Diphthongs are atypical and there is variation in the presence of hiatus. Vowel-initial words are rare and are usually avoided by inserting a glottal stop, \textipa{[\textglotstop]}. Unstressed vowels tend to be deleted.

\textcite{Bennet2015phonology} presents in detail the consonant inventory of modern-day Mayan languages, along with a much more detailed overview of the shared phonology of the language family. A salient trait of Mayan languages that has been widely studied is the contrast of voiceless stops with glottalised stops at the same place of articulation (eg /p/ and /p'/, /k/ and /k'/). 

Stress is present both fixed and mobile, though it is only phonemically contrasted in the Chontal language. Incipient instances of contrastive tone is reported in some languages.

\subsubsection{Morphosyntax} % (fold)
\label{ssub:morphosyntax}

Mayan languages are ergative-absolutive, which means the subject of an intransitive verb is syntactically marked like the object of a transitive verb; as a result, the “subjects” of transitive and intransitive verbs are not identical, which stands in contrast with languages that follow a nominative-accusative alignment, like Spanish or English. Classifier systems abound throughout the family and can either modify noun or numerals.

Word order is verb-initial, but there is room for variation for the purposes of pragmatics and discourse. Pro-drop, where some classes of pronouns are omitted if they are inferable, is allowed and plays an important role in determining word order. \textcite{coon2015morphosyntax} and \textcite{polian2017morphology} present a comprehensive summary of many other features that require more in-depth analysis and training.

\subsubsection{Semantics} % (fold)
\label{ssub:semantics}

Verb roots in Mayan languages are usually polycategorical, meaning they may accept transitive, intransitive or positional derivations. Questions are formed with either intonation, question particles, or interrogative pronouns. Topicalisation and focus often trigger different word order than the neutral verb-initial arrangement, and they might also use a focus particle, depending on whether the focus is informational or identificational. \textcite{henderson2015semantics} provides a much more in-depth and relatively accessible description of the work done with more complex subjects.